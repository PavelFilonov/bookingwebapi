package com.edu.bookingwebapi.dataAccess.mapper;

import com.edu.bookingwebapi.dataAccess.dal.BookingDAL;
import com.edu.bookingwebapi.domain.entity.Booking;
import com.edu.bookingwebapi.domain.mapper.BookingMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class BookingDALMapper implements BookingMapper<BookingDAL> {

    private final UserDALMapper userDALMapper;
    private final RoomDALMapper roomDALMapper;

    @Autowired
    public BookingDALMapper(UserDALMapper userDALMapper, RoomDALMapper roomDALMapper) {
        this.userDALMapper = userDALMapper;
        this.roomDALMapper = roomDALMapper;
    }

    @Override
    public BookingDAL map(Booking booking) {
        if (booking == null)
            return null;

        return BookingDAL.builder()
                .id(booking.getId())
                .comment(booking.getComment())
                .dateFrom(booking.getDateFrom())
                .dateTo(booking.getDateTo())
                .userDAL(userDALMapper.map(booking.getUser()))
                .roomsDAL(roomDALMapper.mapListToDAL(booking.getRooms().stream().collect(Collectors.toList())))
                .build();
    }

    @Override
    public Booking map(BookingDAL bookingDAL) {
        if (bookingDAL == null)
            return null;

        return Booking.builder()
                .id(bookingDAL.getId())
                .comment(bookingDAL.getComment())
                .dateFrom(bookingDAL.getDateFrom())
                .dateTo(bookingDAL.getDateTo())
                .user(userDALMapper.map(bookingDAL.getUserDAL()))
                .rooms(roomDALMapper.mapListToEntity(bookingDAL.getRoomsDAL().stream().collect(Collectors.toList())))
                .build();
    }

    public List<Booking> mapListToEntity(List<BookingDAL> bookingsDAL) {
        List<Booking> bookings = new ArrayList<>();
        bookingsDAL.forEach(bookingDAL -> bookings.add(map(bookingDAL)));
        return bookings;
    }

    public List<BookingDAL> mapListToDAL(List<Booking> bookings) {
        List<BookingDAL> bookingsDAL = new ArrayList<>();
        bookings.forEach(booking -> bookingsDAL.add(map(booking)));
        return bookingsDAL;
    }
}
