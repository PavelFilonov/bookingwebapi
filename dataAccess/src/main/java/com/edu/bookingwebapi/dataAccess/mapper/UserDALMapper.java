package com.edu.bookingwebapi.dataAccess.mapper;

import com.edu.bookingwebapi.dataAccess.dal.UserDAL;
import com.edu.bookingwebapi.domain.entity.User;
import com.edu.bookingwebapi.domain.mapper.UserMapper;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserDALMapper implements UserMapper<UserDAL> {

    @Override
    public UserDAL map(User user) {
        if (user == null)
            return null;

        return UserDAL.builder()
                .id(user.getId())
                .username(user.getUsername())
                .password(user.getPassword())
                .roles(user.getRoles())
                .build();
    }

    @Override
    public User map(UserDAL userDAL) {
        if (userDAL == null)
            return null;

        return User.builder()
                .id(userDAL.getId())
                .username(userDAL.getUsername())
                .password(userDAL.getPassword())
                .roles(userDAL.getRoles())
                .build();
    }

    public List<User> mapListToEntity(List<UserDAL> usersDAL) {
        List<User> users = new ArrayList<>();
        usersDAL.forEach(userDAL -> users.add(map(userDAL)));
        return users;
    }
}
