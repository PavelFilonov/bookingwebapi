package com.edu.bookingwebapi.dataAccess.dal;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "payment")
@Builder
public class PaymentDAL {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id", nullable = false)
    private UserDAL userDAL;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "booking_id", nullable = false)
    private BookingDAL bookingDAL;

    @Column(name = "total_cost", nullable = false)
    private Double totalCost;

    @Column(name = "created_at", nullable = false)
    private LocalDateTime createdAt;
}
