package com.edu.bookingwebapi.dataAccess.mapper;

import com.edu.bookingwebapi.dataAccess.dal.RoomDAL;
import com.edu.bookingwebapi.domain.entity.Room;
import com.edu.bookingwebapi.domain.mapper.RoomMapper;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class RoomDALMapper implements RoomMapper<RoomDAL> {

    @Override
    public RoomDAL map(Room room) {
        if (room == null)
            return null;

        return RoomDAL.builder()
                .id(room.getId())
                .number(room.getNumber())
                .numberRooms(room.getNumberRooms())
                .description(room.getDescription())
                .cost(room.getCost())
                .build();
    }

    @Override
    public Room map(RoomDAL roomDAL) {
        if (roomDAL == null)
            return null;

        return Room.builder()
                .id(roomDAL.getId())
                .number(roomDAL.getNumber())
                .numberRooms(roomDAL.getNumberRooms())
                .description(roomDAL.getDescription())
                .cost(roomDAL.getCost())
                .build();
    }

    public List<Room> mapListToEntity(List<RoomDAL> roomsDAL) {
        List<Room> rooms = new ArrayList<>();
        roomsDAL.forEach(roomDAL -> rooms.add(map(roomDAL)));
        return rooms;
    }

    public List<RoomDAL> mapListToDAL(List<Room> rooms) {
        List<RoomDAL> roomsDAL = new ArrayList<>();
        rooms.forEach(room -> roomsDAL.add(map(room)));
        return roomsDAL;
    }
}
