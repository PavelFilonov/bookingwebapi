package com.edu.bookingwebapi.dataAccess.repository.jpa;

import com.edu.bookingwebapi.dataAccess.dal.PaymentDAL;
import com.edu.bookingwebapi.dataAccess.dal.UserDAL;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JpaPaymentRepository extends JpaRepository<PaymentDAL, Long> {
    List<PaymentDAL> findByUserDAL(UserDAL userDAL);
}
