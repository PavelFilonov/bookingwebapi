package com.edu.bookingwebapi.dataAccess.repository.impl;

import com.edu.bookingwebapi.dataAccess.dal.RoomDAL;
import com.edu.bookingwebapi.dataAccess.mapper.RoomDALMapper;
import com.edu.bookingwebapi.dataAccess.repository.jpa.JpaRoomRepository;
import com.edu.bookingwebapi.domain.entity.Room;
import com.edu.bookingwebapi.domain.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Component
public class RoomRepositoryImpl implements RoomRepository {

    private final JpaRoomRepository jpaRoomRepository;
    private final RoomDALMapper roomDALMapper;

    @Autowired
    public RoomRepositoryImpl(JpaRoomRepository jpaRoomRepository, RoomDALMapper roomDALMapper) {
        this.jpaRoomRepository = jpaRoomRepository;
        this.roomDALMapper = roomDALMapper;
    }

    @Override
    public List<Room> findAll() {
        return roomDALMapper.mapListToEntity(jpaRoomRepository.findAll());
    }

    @Override
    public Room save(Room room) {
        return roomDALMapper.map(jpaRoomRepository.save(roomDALMapper.map(room)));
    }

    @Override
    public Optional<Room> findById(Long id) {
        RoomDAL optionalRoomDAL = jpaRoomRepository.findById(id).orElse(null);
        return Optional.ofNullable(roomDALMapper.map(optionalRoomDAL));
    }

    @Override
    public Room findByNumber(Integer number) {
        return roomDALMapper.map(jpaRoomRepository.findByNumber(number));
    }

    @Override
    public void delete(Long id) {
        jpaRoomRepository.deleteById(id);
    }

    @Override
    public List<Room> findByNumberRooms(Integer numberRooms) {
        return roomDALMapper.mapListToEntity(jpaRoomRepository.findByNumberRooms(numberRooms));
    }

    @Override
    public List<Room> findAvailableRooms(LocalDateTime dateFrom, LocalDateTime dateTo) {
        return roomDALMapper.mapListToEntity(jpaRoomRepository.findAvailableRooms(dateFrom, dateTo));
    }
}
