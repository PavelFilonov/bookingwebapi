package com.edu.bookingwebapi.dataAccess.repository.jpa;

import com.edu.bookingwebapi.dataAccess.dal.RoomDAL;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface JpaRoomRepository extends JpaRepository<RoomDAL, Long> {
    RoomDAL findByNumber(Integer number);

    List<RoomDAL> findByNumberRooms(Integer numberRooms);

    @Query(value =  "select *" +
                    "   from room" +
                    "   where id not in" +
                    "           (" +
                    "           select r.id" +
                    "               from room r" +
                    "                   join booking_room br on r.id = br.room_id" +
                    "                   join booking b on b.id = br.booking_id" +
                    "               where" +
                    "                   (select EXTRACT(EPOCH FROM (date_from - ?1))) > 0 and"  +
                    "                   (select EXTRACT(EPOCH FROM (?2 - date_to))) > 0" +
                    "                   or" +
                    "                   (select EXTRACT(EPOCH FROM (?1 - date_from))) > 0 and"  +
                    "                   (select EXTRACT(EPOCH FROM (date_to - ?2))) > 0" +
                    "                   or" +
                    "                   (select EXTRACT(EPOCH FROM (?1 - date_from))) >= 0 and" +
                    "                   (select EXTRACT(EPOCH FROM (date_to - ?1))) >= 0" +
                    "                   or" +
                    "                   (select EXTRACT(EPOCH FROM (date_to - ?2))) >= 0 and" +
                    "                   (select EXTRACT(EPOCH FROM (?2 - date_from))) >= 0" +
                    "           )",
            nativeQuery = true)
    List<RoomDAL> findAvailableRooms(LocalDateTime dateFrom, LocalDateTime dateTo);
}
