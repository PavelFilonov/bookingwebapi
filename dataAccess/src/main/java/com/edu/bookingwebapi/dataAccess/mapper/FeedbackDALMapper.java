package com.edu.bookingwebapi.dataAccess.mapper;

import com.edu.bookingwebapi.dataAccess.dal.FeedbackDAL;
import com.edu.bookingwebapi.domain.entity.Feedback;
import com.edu.bookingwebapi.domain.mapper.FeedbackMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class FeedbackDALMapper implements FeedbackMapper<FeedbackDAL> {

    private final UserDALMapper userDALMapper;
    private final RoomDALMapper roomDALMapper;

    @Autowired
    public FeedbackDALMapper(UserDALMapper userDALMapper, RoomDALMapper roomDALMapper) {
        this.userDALMapper = userDALMapper;
        this.roomDALMapper = roomDALMapper;
    }

    @Override
    public FeedbackDAL map(Feedback feedback) {
        if (feedback == null)
            return null;

        return FeedbackDAL.builder()
                .id(feedback.getId())
                .userDAL(userDALMapper.map(feedback.getUser()))
                .roomDAL(roomDALMapper.map(feedback.getRoom()))
                .description(feedback.getDescription())
                .createdAt(feedback.getCreatedAt())
                .build();
    }

    @Override
    public Feedback map(FeedbackDAL feedbackDAL) {
        if (feedbackDAL == null)
            return null;

        return Feedback.builder()
                .id(feedbackDAL.getId())
                .user(userDALMapper.map(feedbackDAL.getUserDAL()))
                .room(roomDALMapper.map(feedbackDAL.getRoomDAL()))
                .description(feedbackDAL.getDescription())
                .createdAt(feedbackDAL.getCreatedAt())
                .build();
    }

    public List<Feedback> mapListToEntity(List<FeedbackDAL> feedbacksDAL) {
        List<Feedback> feedbacks = new ArrayList<>();
        feedbacksDAL.forEach(feedbackDAL -> feedbacks.add(map(feedbackDAL)));
        return feedbacks;
    }
}
