package com.edu.bookingwebapi.dataAccess.repository.impl;

import com.edu.bookingwebapi.dataAccess.dal.BookingDAL;
import com.edu.bookingwebapi.dataAccess.mapper.BookingDALMapper;
import com.edu.bookingwebapi.dataAccess.mapper.UserDALMapper;
import com.edu.bookingwebapi.dataAccess.repository.jpa.JpaBookingRepository;
import com.edu.bookingwebapi.domain.entity.Booking;
import com.edu.bookingwebapi.domain.entity.User;
import com.edu.bookingwebapi.domain.repository.BookingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BookingRepositoryImpl implements BookingRepository {

    private final JpaBookingRepository jpaBookingRepository;
    private final UserDALMapper userDALMapper;
    private final BookingDALMapper bookingDALMapper;

    @Autowired
    public BookingRepositoryImpl(JpaBookingRepository jpaBookingRepository, UserDALMapper userDALMapper, BookingDALMapper bookingDALMapper) {
        this.jpaBookingRepository = jpaBookingRepository;
        this.userDALMapper = userDALMapper;
        this.bookingDALMapper = bookingDALMapper;
    }

    @Override
    public List<Booking> findBookingsByUser(User user) {
        List<BookingDAL> bookingsDal = jpaBookingRepository.findBookingsByUserDAL(userDALMapper.map(user));
        return bookingDALMapper.mapListToEntity(bookingsDal);
    }

    @Override
    public List<Booking> findBookingsByRoomId(Long id) {
        List<BookingDAL> bookingsDal = jpaBookingRepository.findBookingsByRoomID(id);
        return bookingDALMapper.mapListToEntity(bookingsDal);
    }

    @Override
    public Booking save(Booking booking) {
        return bookingDALMapper.map(jpaBookingRepository.save(bookingDALMapper.map(booking)));
    }
}