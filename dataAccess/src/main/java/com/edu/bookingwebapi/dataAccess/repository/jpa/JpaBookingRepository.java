package com.edu.bookingwebapi.dataAccess.repository.jpa;

import com.edu.bookingwebapi.dataAccess.dal.BookingDAL;
import com.edu.bookingwebapi.dataAccess.dal.UserDAL;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JpaBookingRepository extends JpaRepository<BookingDAL, Long> {
    List<BookingDAL> findBookingsByUserDAL(UserDAL userDAL);

    @Query(value =  "select id, comment, date_from, date_to, user_id " +
                    "   from booking b join booking_room b_r on b.id = b_r.booking_id " +
                    "   where room_id = ?1", nativeQuery = true)
    List<BookingDAL> findBookingsByRoomID(Long id);
}
