package com.edu.bookingwebapi.dataAccess.repository.impl;

import com.edu.bookingwebapi.dataAccess.mapper.FeedbackDALMapper;
import com.edu.bookingwebapi.dataAccess.mapper.RoomDALMapper;
import com.edu.bookingwebapi.dataAccess.repository.jpa.JpaFeedbackRepository;
import com.edu.bookingwebapi.domain.entity.Feedback;
import com.edu.bookingwebapi.domain.entity.Room;
import com.edu.bookingwebapi.domain.repository.FeedbackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class FeedbackRepositoryImpl implements FeedbackRepository {

    private final JpaFeedbackRepository jpaFeedbackRepository;
    private final FeedbackDALMapper feedbackDALMapper;
    private final RoomDALMapper roomDALMapper;

    @Autowired
    public FeedbackRepositoryImpl(JpaFeedbackRepository jpaFeedbackRepository, FeedbackDALMapper feedbackDALMapper,
                                  RoomDALMapper roomDALMapper) {
        this.jpaFeedbackRepository = jpaFeedbackRepository;
        this.feedbackDALMapper = feedbackDALMapper;
        this.roomDALMapper = roomDALMapper;
    }

    @Override
    public Feedback save(Feedback feedback) {
        return feedbackDALMapper.map(jpaFeedbackRepository.save(feedbackDALMapper.map(feedback)));
    }

    @Override
    public List<Feedback> findByRoom(Room room) {
        return feedbackDALMapper.mapListToEntity(jpaFeedbackRepository.findByRoomDAL(roomDALMapper.map(room)));
    }

    @Override
    public Feedback findById(Long id) {
        return feedbackDALMapper.map(jpaFeedbackRepository.findById(id).orElse(null));
    }

    @Override
    public void delete(Long id) {
        jpaFeedbackRepository.deleteById(id);
    }
}
