package com.edu.bookingwebapi.dataAccess.dal;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "room")
@Builder
public class RoomDAL {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private Integer number;

    @Column(name = "number_rooms", nullable = false)
    private Integer numberRooms;

    private String description;

    @Column(nullable = false)
    private Double cost;
}
