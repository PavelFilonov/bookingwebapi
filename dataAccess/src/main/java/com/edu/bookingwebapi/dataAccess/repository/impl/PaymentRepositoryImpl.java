package com.edu.bookingwebapi.dataAccess.repository.impl;

import com.edu.bookingwebapi.dataAccess.mapper.PaymentDALMapper;
import com.edu.bookingwebapi.dataAccess.mapper.UserDALMapper;
import com.edu.bookingwebapi.dataAccess.repository.jpa.JpaPaymentRepository;
import com.edu.bookingwebapi.domain.entity.Payment;
import com.edu.bookingwebapi.domain.entity.User;
import com.edu.bookingwebapi.domain.repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PaymentRepositoryImpl implements PaymentRepository {

    private final JpaPaymentRepository jpaPaymentRepository;
    private final PaymentDALMapper paymentDALMapper;
    private final UserDALMapper userDALMapper;

    @Autowired
    public PaymentRepositoryImpl(JpaPaymentRepository jpaPaymentRepository, PaymentDALMapper paymentDALMapper,
                                 UserDALMapper userDALMapper) {
        this.jpaPaymentRepository = jpaPaymentRepository;
        this.paymentDALMapper = paymentDALMapper;
        this.userDALMapper = userDALMapper;
    }

    @Override
    public Payment save(Payment payment) {
        return paymentDALMapper.map(jpaPaymentRepository.save(paymentDALMapper.map(payment)));
    }

    @Override
    public List<Payment> findByUser(User user) {
        return paymentDALMapper.mapListToEntity(jpaPaymentRepository.findByUserDAL(userDALMapper.map(user)));
    }
}
