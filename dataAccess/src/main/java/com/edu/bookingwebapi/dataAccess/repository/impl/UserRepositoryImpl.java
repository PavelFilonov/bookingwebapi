package com.edu.bookingwebapi.dataAccess.repository.impl;

import com.edu.bookingwebapi.dataAccess.mapper.UserDALMapper;
import com.edu.bookingwebapi.dataAccess.repository.jpa.JpaUserRepository;
import com.edu.bookingwebapi.domain.entity.User;
import com.edu.bookingwebapi.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserRepositoryImpl implements UserRepository {

    private final JpaUserRepository jpaUserRepository;
    private final UserDALMapper userDALMapper;

    @Autowired
    public UserRepositoryImpl(JpaUserRepository jpaUserRepository, UserDALMapper userDALMapper) {
        this.jpaUserRepository = jpaUserRepository;
        this.userDALMapper = userDALMapper;
    }

    @Override
    public User findUserByUsername(String username) {
        return userDALMapper.map(jpaUserRepository.findUserByUsername(username));
    }

    @Override
    public boolean existsUserByUsername(String username) {
        return jpaUserRepository.existsUserByUsername(username);
    }

    @Override
    public User save(User user) {
        return userDALMapper.map(jpaUserRepository.save(userDALMapper.map(user)));
    }

    @Override
    public void updateUsername(String username, Long id) {
        jpaUserRepository.updateUsername(username, id);
    }

    @Override
    public void updatePassword(String password, Long id) {
        jpaUserRepository.updatePassword(password, id);
    }

    @Override
    public User findById(Long id) {
        return userDALMapper.map(jpaUserRepository.findById(id).orElse(null));
    }

    @Override
    public void deleteById(Long id) {
        jpaUserRepository.deleteById(id);
    }

    @Override
    public List<User> findAll() {
        return userDALMapper.mapListToEntity(jpaUserRepository.findAll());
    }

    @Override
    public void deleteByUsername(String username) {
        jpaUserRepository.deleteByUsername(username);
    }
}
