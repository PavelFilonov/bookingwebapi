package com.edu.bookingwebapi.dataAccess.repository.jpa;

import com.edu.bookingwebapi.dataAccess.dal.FeedbackDAL;
import com.edu.bookingwebapi.dataAccess.dal.RoomDAL;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JpaFeedbackRepository extends JpaRepository<FeedbackDAL, Long> {
    List<FeedbackDAL> findByRoomDAL(RoomDAL roomDAL);
}
