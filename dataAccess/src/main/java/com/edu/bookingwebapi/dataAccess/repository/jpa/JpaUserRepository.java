package com.edu.bookingwebapi.dataAccess.repository.jpa;

import com.edu.bookingwebapi.dataAccess.dal.UserDAL;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface JpaUserRepository extends JpaRepository<UserDAL, Long> {
    UserDAL findUserByUsername(String username);

    boolean existsUserByUsername(String username);

    @Transactional
    @Modifying
    @Query(value =  "update user1      " +
                    "set username = ?1 " +
                    "where id = ?2     ",
            nativeQuery = true)
    void updateUsername(String username, Long id);

    @Transactional
    @Modifying
    @Query(value =  "update user1      " +
                    "set password = ?1 " +
                    "where id = ?2     ",
            nativeQuery = true)
    void updatePassword(String password, Long id);


    @Transactional
    @Modifying
    @Query(value =  "delete from user1   " +
                    "where username = ?1 ",
            nativeQuery = true)
    void deleteByUsername(String username);
}
