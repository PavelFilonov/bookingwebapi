package com.edu.bookingwebapi.dataAccess.mapper;

import com.edu.bookingwebapi.dataAccess.dal.PaymentDAL;
import com.edu.bookingwebapi.domain.entity.Payment;
import com.edu.bookingwebapi.domain.mapper.PaymentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PaymentDALMapper implements PaymentMapper<PaymentDAL> {

    private final UserDALMapper userDALMapper;
    private final BookingDALMapper bookingDALMapper;

    @Autowired
    public PaymentDALMapper(UserDALMapper userDALMapper, BookingDALMapper bookingDALMapper) {
        this.userDALMapper = userDALMapper;
        this.bookingDALMapper = bookingDALMapper;
    }

    @Override
    public PaymentDAL map(Payment payment) {
        if (payment == null)
            return null;

        return PaymentDAL.builder()
                .id(payment.getId())
                .userDAL(userDALMapper.map(payment.getUser()))
                .bookingDAL(bookingDALMapper.map(payment.getBooking()))
                .totalCost(payment.getTotalCost())
                .createdAt(payment.getCreatedAt())
                .build();
    }

    @Override
    public Payment map(PaymentDAL paymentDAL) {
        if (paymentDAL == null)
            return null;

        return Payment.builder()
                .id(paymentDAL.getId())
                .user(userDALMapper.map(paymentDAL.getUserDAL()))
                .booking(bookingDALMapper.map(paymentDAL.getBookingDAL()))
                .totalCost(paymentDAL.getTotalCost())
                .createdAt(paymentDAL.getCreatedAt())
                .build();
    }

    public List<Payment> mapListToEntity(List<PaymentDAL> paymentsDAL) {
        List<Payment> payments = new ArrayList<>();
        paymentsDAL.forEach(paymentDAL -> payments.add(map(paymentDAL)));
        return payments;
    }
}
