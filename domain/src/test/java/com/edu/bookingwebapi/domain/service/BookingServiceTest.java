package com.edu.bookingwebapi.domain.service;

import com.edu.bookingwebapi.domain.entity.Booking;
import com.edu.bookingwebapi.domain.entity.Room;
import com.edu.bookingwebapi.domain.entity.User;
import com.edu.bookingwebapi.domain.exception.NoSuchRoomException;
import com.edu.bookingwebapi.domain.exception.NotAvailableRoomException;
import com.edu.bookingwebapi.domain.repository.BookingRepository;
import com.edu.bookingwebapi.domain.service.impl.BookingServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class BookingServiceTest {
    private BookingService bookingService;

    @Mock
    private BookingRepository bookingRepository;

    @Mock
    private RoomService roomService;

    private final LocalDateTime fromDateTime = LocalDateTime.of(2022, 4, 5, 12, 30);
    private final LocalDateTime toDateTime = LocalDateTime.of(2022, 4, 5, 12, 30);

    @BeforeEach
    void setUp() {
        bookingService = new BookingServiceImpl(bookingRepository, roomService);
    }

    @Test
    void findBookingsByUser() {
        // setup
        var user = User.builder().id(1L).build();
        var booking = Booking.builder().id(1L).build();
        var bookings = Collections.singletonList(booking);
        when(bookingRepository.findBookingsByUser(user)).thenReturn(bookings);

        // test execution
        var bookingsByUser = bookingService.findBookingsByUser(user);

        // test check
        assertThat(bookingsByUser).isSameAs(bookings);
        verify(bookingRepository).findBookingsByUser(user);
    }

    @Test
    public void throwsErrorWhenRoomIsNotAvailable() {
        // setup
        var room = Room.builder()
            .id(1L)
            .build();
        var booking = Booking.builder()
            .id(2L)
            .dateFrom(fromDateTime)
            .dateTo(toDateTime)
            .rooms(Collections.singleton(room))
            .build();
        lenient()
            .when(roomService.isRoomAvailable(room, fromDateTime, toDateTime))
            .thenReturn(false);

        // test execution and check
        assertThatThrownBy(() -> bookingService.save(booking))
            .isExactlyInstanceOf(NotAvailableRoomException.class);
    }

    @Test
    public void savedToRepositoryWhenRoomIsAvailable() throws NotAvailableRoomException {
        // setup
        var room = Room.builder()
            .id(1L)
            .build();
        var booking = Booking.builder()
            .id(2L)
            .dateFrom(fromDateTime)
            .dateTo(toDateTime)
            .rooms(Collections.singleton(room))
            .build();
        lenient()
            .when(roomService.isRoomAvailable(room, fromDateTime, toDateTime))
            .thenReturn(true);
        when(bookingRepository.save(booking)).thenReturn(booking);
        InOrder inOrder = inOrder(roomService, bookingRepository);

        // test execution
        var resultBooking = bookingService.save(booking);

        // test check
        assertThat(resultBooking).isSameAs(booking);
        inOrder.verify(roomService).isRoomAvailable(room, fromDateTime, toDateTime);
        inOrder.verify(bookingRepository).save(booking);
    }

    @Test
    public void throwsExceptionWhenBookingDoesNotContainRoom() {
        // setup
        Booking booking = Booking.builder()
            .id(2L)
            .dateFrom(fromDateTime)
            .dateTo(toDateTime)
            .build();

        // test execution and check
        assertThatThrownBy(() -> bookingService.save(booking))
            .isExactlyInstanceOf(NoSuchRoomException.class);
    }
}