package com.edu.bookingwebapi.domain.service;

import com.edu.bookingwebapi.domain.entity.Role;
import com.edu.bookingwebapi.domain.entity.User;
import com.edu.bookingwebapi.domain.repository.UserRepository;
import com.edu.bookingwebapi.domain.service.impl.UserServiceIml;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    private UserService userService;

    @Mock
    private UserRepository userRepository;

    private User user;

    @BeforeEach
    void setUp() {
        user = User.builder()
                .id(1L)
                .username("Pavel")
                .password("123")
                .roles(Collections.singleton(Role.USER))
                .build();
        lenient().when(userRepository.findUserByUsername(eq("Pavel"))).thenReturn(user);

        userService = new UserServiceIml(userRepository);
    }

    @Test
    void findByUsername() {
        // test execution
        var user = userService.findByUsername("Pavel");

        // test check
        assertThat(user).isSameAs(this.user);
        verify(userRepository).findUserByUsername("Pavel");
    }

    @Test
    void existsByUsername() {
        // setup
        when(userRepository.existsUserByUsername(eq("Pavel"))).thenReturn(true);

        // test execution
        boolean exists = userService.existsByUsername("Pavel");

        // test check
        assertTrue(exists);
        verify(userRepository).existsUserByUsername(anyString());
    }

    @Test
    void notExistsByUsername() {
        // test execution
        boolean exists = userService.existsByUsername("Error");

        // test check
        assertFalse(exists);
        verify(userRepository).existsUserByUsername(anyString());
    }

    @Test
    void save() {
        // setup
        when(userRepository.save(user)).thenReturn(user);

        // test execution
        var user = userService.save(this.user);

        // test check
        assertThat(user).isSameAs(this.user);
        verify(userRepository).save(this.user);
    }

    @Test
    void loadUserByUsernameWhenUserIsNotNull() {
        // setup
        var user = new org.springframework.security.core.userdetails.User(
                this.user.getUsername(),
                this.user.getPassword(),
                Collections.singleton(new SimpleGrantedAuthority(this.user.getRoles().iterator().next().toString()))
        );

        // test execution
        UserDetails userDetails = userService.loadUserByUsername("Pavel");

        // test check
        assertEquals(user, userDetails);
        verify(userRepository).findUserByUsername("Pavel");
    }

    @Test
    void throwsExceptionWhenUserIsNull() {
        // setup
        lenient().when(userRepository.findUserByUsername(eq(null))).thenReturn(null);

        // test execution and check
        assertThatThrownBy(() -> userService.loadUserByUsername(null))
                .isExactlyInstanceOf(UsernameNotFoundException.class);
    }
}