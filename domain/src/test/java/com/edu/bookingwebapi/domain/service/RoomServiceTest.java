package com.edu.bookingwebapi.domain.service;

import com.edu.bookingwebapi.domain.entity.Booking;
import com.edu.bookingwebapi.domain.entity.Room;
import com.edu.bookingwebapi.domain.exception.NoSuchRoomException;
import com.edu.bookingwebapi.domain.repository.BookingRepository;
import com.edu.bookingwebapi.domain.repository.RoomRepository;
import com.edu.bookingwebapi.domain.service.impl.RoomServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RoomServiceTest {

    private RoomService roomService;

    @Mock
    private RoomRepository roomRepository;

    @Mock
    private BookingRepository bookingRepository;

    private Room room;
    private List<Room> rooms;
    private final LocalDateTime fromDateTime = LocalDateTime.of(2022, 4, 8, 12, 30);
    private final LocalDateTime toDateTime = LocalDateTime.of(2022, 4, 9, 12, 30);

    @BeforeEach
    void setUp() {
        room = Room.builder().id(1L).number(1).build();
        Booking booking = Booking.builder().id(1L)
                .dateFrom(LocalDateTime.of(2022, 4, 5, 12, 30))
                .dateTo(LocalDateTime.of(2022, 4, 7, 12, 30))
                .build();
        rooms = Collections.singletonList(room);

        lenient().when(roomRepository.findAll()).thenReturn(rooms);
        lenient().when(bookingRepository.findBookingsByRoomId(room.getId())).thenReturn(Collections.singletonList(booking));

        roomService = new RoomServiceImpl(roomRepository, bookingRepository);
    }

    @Test
    void findAll() {
        // test execution
        var allRooms = roomService.findAll();

        // test check
        assertThat(allRooms).isSameAs(rooms);
        verify(roomRepository).findAll();
    }

    @Test
    void save() {
        // setup
        lenient().when(roomRepository.save(room)).thenReturn(room);

        // test execution
        var room = roomService.save(this.room);

        // test check
        assertThat(room).isSameAs(this.room);
        verify(roomRepository).save(this.room);
    }

    @Test
    void getRoomsByIdWhenAllRoomsFound() {
        // setup
        when(roomRepository.findById(1L)).thenReturn(Optional.ofNullable(room));

        // test execution
        var rooms = roomService.getRoomsById(Collections.singletonList(1L));

        // test check
        assertEquals(this.rooms, rooms);
        verify(roomRepository).findById(1L);
    }

    @Test
    void throwsExceptionWhenRoomNotFound() {
        // setup
        when(roomRepository.findById(2L)).thenReturn(Optional.empty());

        // test execution and check
        assertThatThrownBy(() -> roomService.getRoomsById(Collections.singletonList(2L)))
                .isExactlyInstanceOf(NoSuchRoomException.class);
    }

    @Test
    void getAvailableRooms() {
        // setup
        InOrder inOrder = inOrder(roomRepository, bookingRepository);

        // test execution
        var availableRooms = roomService.findAvailableRooms(fromDateTime, toDateTime);

        // test check
        assertEquals(rooms, availableRooms);
        inOrder.verify(roomRepository).findAll();
        inOrder.verify(bookingRepository).findBookingsByRoomId(1L);
    }

    @Test
    void isRoomAvailable() {
        // test execution
        boolean available = roomService.isRoomAvailable(room, fromDateTime, toDateTime);

        // test check
        assertTrue(available);
        verify(bookingRepository).findBookingsByRoomId(1L);
    }

    @Test
    void isNotRoomAvailable() {
        // test execution
        boolean available = roomService.isRoomAvailable(room,
                LocalDateTime.of(2022, 4, 5, 12, 30),
                LocalDateTime.of(2022, 4, 6, 12, 30));

        // test check
        assertFalse(available);
        verify(bookingRepository).findBookingsByRoomId(1L);
    }
}