package com.edu.bookingwebapi.domain.service;

import com.edu.bookingwebapi.domain.entity.Room;

import java.time.LocalDateTime;
import java.util.List;

public interface RoomService {
    List<Room> findAll();

    Room save(Room room);

    List<Room> getRoomsById(List<Long> roomsId);

    List<Room> findAvailableRooms(LocalDateTime dateFrom, LocalDateTime dateTo);

    boolean isRoomAvailable(Room room, LocalDateTime dateFrom, LocalDateTime dateTo);

    Room findById(Long id);

    Room findByNumber(Integer number);

    void delete(Long id);

    List<Room> findByNumberRooms(Integer numberRooms);
}
