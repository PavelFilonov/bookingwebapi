package com.edu.bookingwebapi.domain.entity;

import lombok.Builder;
import lombok.Data;

import java.util.Collection;

@Data
@Builder
public class User {
    private Long id;
    private String username;
    private String password;
    private Collection<Role> roles;
}
