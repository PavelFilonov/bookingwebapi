package com.edu.bookingwebapi.domain.entity;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class Feedback {
    private Long id;
    private User user;
    private Room room;
    private String description;
    private LocalDateTime createdAt;
}
