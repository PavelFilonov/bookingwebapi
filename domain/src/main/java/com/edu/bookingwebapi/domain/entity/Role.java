package com.edu.bookingwebapi.domain.entity;

public enum Role {
    USER,
    ADMIN
}
