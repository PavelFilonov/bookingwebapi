package com.edu.bookingwebapi.domain.service;

import com.edu.bookingwebapi.domain.entity.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.List;

public interface UserService extends UserDetailsService {
    User findByUsername(String username);

    boolean existsByUsername(String username);

    User save(User user);

    boolean isAdmin(String username);

    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;

    void updateUsername(String username, Long id);

    void updatePassword(String password, Long id);

    User findById(Long id);

    void deleteById(Long id);

    List<User> findAll();

    void deleteByUsername(String username);
}
