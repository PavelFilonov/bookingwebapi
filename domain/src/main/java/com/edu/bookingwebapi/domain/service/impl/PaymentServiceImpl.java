package com.edu.bookingwebapi.domain.service.impl;

import com.edu.bookingwebapi.domain.entity.Payment;
import com.edu.bookingwebapi.domain.entity.User;
import com.edu.bookingwebapi.domain.repository.PaymentRepository;
import com.edu.bookingwebapi.domain.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentServiceImpl implements PaymentService {

    private final PaymentRepository paymentRepository;

    @Autowired
    public PaymentServiceImpl(PaymentRepository paymentRepository) {
        this.paymentRepository = paymentRepository;
    }

    @Override
    public List<Payment> findByUser(User user) {
        return paymentRepository.findByUser(user);
    }

    @Override
    public Payment save(Payment payment) {
        return paymentRepository.save(payment);
    }
}
