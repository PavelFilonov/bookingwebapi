package com.edu.bookingwebapi.domain.repository;

import com.edu.bookingwebapi.domain.entity.Booking;
import com.edu.bookingwebapi.domain.entity.User;

import java.util.List;

public interface BookingRepository {
    List<Booking> findBookingsByUser(User user);

    List<Booking> findBookingsByRoomId(Long id);

    Booking save(Booking booking);
}
