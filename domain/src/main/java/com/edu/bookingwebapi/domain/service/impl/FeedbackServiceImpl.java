package com.edu.bookingwebapi.domain.service.impl;

import com.edu.bookingwebapi.domain.entity.Feedback;
import com.edu.bookingwebapi.domain.entity.Room;
import com.edu.bookingwebapi.domain.exception.NoSuchFeedbackException;
import com.edu.bookingwebapi.domain.repository.FeedbackRepository;
import com.edu.bookingwebapi.domain.service.FeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FeedbackServiceImpl implements FeedbackService {

    private final FeedbackRepository feedbackRepository;

    @Autowired
    public FeedbackServiceImpl(FeedbackRepository feedbackRepository) {
        this.feedbackRepository = feedbackRepository;
    }

    @Override
    public Feedback save(Feedback feedback) {
        return feedbackRepository.save(feedback);
    }

    @Override
    public List<Feedback> findByRoom(Room room) {
        return feedbackRepository.findByRoom(room);
    }

    @Override
    public Feedback findById(Long id) {
        Feedback feedback = feedbackRepository.findById(id);
        if (feedback == null)
            throw new NoSuchFeedbackException(String.format("Feedback number %s not found", id));
        return feedback;
    }

    @Override
    public void delete(Long id) {
        feedbackRepository.delete(id);
    }
}
