package com.edu.bookingwebapi.domain.entity;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Room {
    private Long id;
    private Integer number;
    private Integer numberRooms;
    private String description;
    private Double cost;
}
