package com.edu.bookingwebapi.domain.mapper;

import com.edu.bookingwebapi.domain.entity.Payment;

public interface PaymentMapper <T> {
    T map(Payment payment);
    Payment map(T obj);
}
