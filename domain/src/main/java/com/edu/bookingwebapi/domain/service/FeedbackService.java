package com.edu.bookingwebapi.domain.service;

import com.edu.bookingwebapi.domain.entity.Feedback;
import com.edu.bookingwebapi.domain.entity.Room;

import java.util.List;

public interface FeedbackService {
    Feedback save(Feedback feedback);

    List<Feedback> findByRoom(Room room);

    Feedback findById(Long id);

    void delete(Long id);
}
