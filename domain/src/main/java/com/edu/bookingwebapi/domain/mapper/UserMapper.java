package com.edu.bookingwebapi.domain.mapper;

import com.edu.bookingwebapi.domain.entity.User;

public interface UserMapper <T> {
    T map(User user);
    User map(T obj);
}
