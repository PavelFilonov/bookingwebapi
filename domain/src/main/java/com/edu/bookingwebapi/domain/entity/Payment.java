package com.edu.bookingwebapi.domain.entity;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class Payment {
    private Long id;
    private User user;
    private Booking booking;
    private Double totalCost;
    private LocalDateTime createdAt;
}
