package com.edu.bookingwebapi.domain.mapper;

import com.edu.bookingwebapi.domain.entity.Feedback;

public interface FeedbackMapper <T> {
    T map(Feedback feedback);
    Feedback map(T obj);
}
