package com.edu.bookingwebapi.domain.mapper;

import com.edu.bookingwebapi.domain.entity.Room;

public interface RoomMapper <T> {
    T map(Room room);
    Room map(T obj);
}
