package com.edu.bookingwebapi.domain.service;

import com.edu.bookingwebapi.domain.entity.Payment;
import com.edu.bookingwebapi.domain.entity.User;

import java.util.List;

public interface PaymentService {
    List<Payment> findByUser(User user);

    Payment save(Payment payment);
}
