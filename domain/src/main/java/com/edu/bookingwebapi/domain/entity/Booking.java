package com.edu.bookingwebapi.domain.entity;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Collection;

@Data
@Builder
public class Booking {
    private Long id;
    private String comment;
    private LocalDateTime dateFrom;
    private LocalDateTime dateTo;
    private User user;
    private Collection<Room> rooms;
}
