package com.edu.bookingwebapi.domain.service;

import com.edu.bookingwebapi.domain.entity.Booking;
import com.edu.bookingwebapi.domain.entity.User;
import com.edu.bookingwebapi.domain.exception.NoSuchRoomException;
import com.edu.bookingwebapi.domain.exception.NotAvailableRoomException;

import java.util.List;

public interface BookingService {
    List<Booking> findBookingsByUser(User user);

    Booking save(Booking booking) throws NoSuchRoomException, NotAvailableRoomException;
}
