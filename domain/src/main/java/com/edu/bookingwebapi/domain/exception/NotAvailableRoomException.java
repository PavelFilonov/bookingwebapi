package com.edu.bookingwebapi.domain.exception;

public class NotAvailableRoomException extends Exception {
    public NotAvailableRoomException(String message) {
        super(message);
    }
}
