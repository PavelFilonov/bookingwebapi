package com.edu.bookingwebapi.domain.service.impl;

import com.edu.bookingwebapi.domain.entity.Booking;
import com.edu.bookingwebapi.domain.entity.Room;
import com.edu.bookingwebapi.domain.exception.NoSuchRoomException;
import com.edu.bookingwebapi.domain.repository.BookingRepository;
import com.edu.bookingwebapi.domain.repository.RoomRepository;
import com.edu.bookingwebapi.domain.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class RoomServiceImpl implements RoomService {

    private final RoomRepository roomRepository;
    private final BookingRepository bookingRepository;

    @Autowired
    public RoomServiceImpl(RoomRepository roomRepository, BookingRepository bookingRepository) {
        this.roomRepository = roomRepository;
        this.bookingRepository = bookingRepository;
    }

    @Override
    public List<Room> findAll() {
        return roomRepository.findAll();
    }

    @Override
    public Room save(Room room) {
        return roomRepository.save(room);
    }

    @Override
    public List<Room> getRoomsById(List<Long> roomsId) {
        List<Room> rooms = new ArrayList<>();

        for (Long id : roomsId) {
            Optional<Room> room = roomRepository.findById(id);
            if (room.isEmpty())
                throw new NoSuchRoomException(String.format("Room id %s not found", id));

            rooms.add(room.get());
        }

        return rooms;
    }

    @Override
    public List<Room> findAvailableRooms(LocalDateTime dateFrom, LocalDateTime dateTo) {
        return roomRepository.findAvailableRooms(dateFrom, dateTo);
    }

    @Override
    public boolean isRoomAvailable(Room room, LocalDateTime dateFrom, LocalDateTime dateTo) {
        Collection<Booking> bookings = bookingRepository.findBookingsByRoomId(room.getId());

        for (Booking b : bookings) {
            long secondsBookingFrom2From = Duration.between(b.getDateFrom(), dateFrom).toSeconds();
            long secondsBookingFrom2To = Duration.between(b.getDateFrom(), dateTo).toSeconds();
            long secondsFrom2BookingTo = Duration.between(dateFrom, b.getDateTo()).toSeconds();
            long secondsTo2BookingTo = Duration.between(dateTo, b.getDateTo()).toSeconds();

            if (dateFrom.isBefore(b.getDateFrom()) && dateTo.isAfter(b.getDateTo())
                || dateFrom.isAfter(b.getDateFrom()) && dateTo.isBefore(b.getDateTo())
                || secondsBookingFrom2From >= 0 && secondsFrom2BookingTo >= 0
                || secondsTo2BookingTo >= 0 && secondsBookingFrom2To >= 0
                )
                return false;
        }

        return true;
    }

    @Override
    public Room findById(Long id) {
        Optional<Room> room = roomRepository.findById(id);
        if (room.isEmpty())
            throw new NoSuchRoomException(String.format("Room id %s not found", id));
        return room.get();
    }

    @Override
    public Room findByNumber(Integer number) {
        Room room = roomRepository.findByNumber(number);
        if (room == null)
            throw new NoSuchRoomException(String.format("Room number %s not found", number));
        return room;
    }

    @Override
    public void delete(Long id) {
        roomRepository.delete(id);
    }

    @Override
    public List<Room> findByNumberRooms(Integer numberRooms) {
        return roomRepository.findByNumberRooms(numberRooms);
    }
}
