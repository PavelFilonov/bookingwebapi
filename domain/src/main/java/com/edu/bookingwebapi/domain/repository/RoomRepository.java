package com.edu.bookingwebapi.domain.repository;

import com.edu.bookingwebapi.domain.entity.Room;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface RoomRepository {
    List<Room> findAll();

    Room save(Room room);

    Optional<Room> findById(Long id);

    Room findByNumber(Integer number);

    void delete(Long id);

    List<Room> findByNumberRooms(Integer numberRooms);

    List<Room> findAvailableRooms(LocalDateTime dateFrom, LocalDateTime dateTo);
}
