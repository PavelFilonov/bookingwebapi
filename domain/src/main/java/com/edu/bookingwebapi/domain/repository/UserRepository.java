package com.edu.bookingwebapi.domain.repository;

import com.edu.bookingwebapi.domain.entity.User;

import java.util.List;

public interface UserRepository {
    User findUserByUsername(String username);

    boolean existsUserByUsername(String username);

    User save(User user);

    void updateUsername(String username, Long id);

    void updatePassword(String password, Long id);

    User findById(Long id);

    void deleteById(Long id);

    List<User> findAll();

    void deleteByUsername(String username);
}
