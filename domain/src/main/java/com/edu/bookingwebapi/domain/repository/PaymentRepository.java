package com.edu.bookingwebapi.domain.repository;

import com.edu.bookingwebapi.domain.entity.Payment;
import com.edu.bookingwebapi.domain.entity.User;

import java.util.List;

public interface PaymentRepository {
    Payment save(Payment payment);

    List<Payment> findByUser(User user);
}
