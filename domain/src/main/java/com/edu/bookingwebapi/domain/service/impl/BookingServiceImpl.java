package com.edu.bookingwebapi.domain.service.impl;

import com.edu.bookingwebapi.domain.entity.Booking;
import com.edu.bookingwebapi.domain.entity.Room;
import com.edu.bookingwebapi.domain.entity.User;
import com.edu.bookingwebapi.domain.exception.NoSuchRoomException;
import com.edu.bookingwebapi.domain.exception.NotAvailableRoomException;
import com.edu.bookingwebapi.domain.repository.BookingRepository;
import com.edu.bookingwebapi.domain.service.BookingService;
import com.edu.bookingwebapi.domain.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class BookingServiceImpl implements BookingService {

    private final BookingRepository bookingRepository;
    private final RoomService roomService;

    @Autowired
    public BookingServiceImpl(BookingRepository bookingRepository, RoomService roomService) {
        this.bookingRepository = bookingRepository;
        this.roomService = roomService;
    }

    @Override
    public List<Booking> findBookingsByUser(User user) {
        return bookingRepository.findBookingsByUser(user);
    }

    @Override
    public Booking save(Booking booking) throws NoSuchRoomException, NotAvailableRoomException {
        if (Objects.isNull(booking.getRooms())
            || booking.getRooms().isEmpty())
            throw new NoSuchRoomException("Cannot create a booking without rooms");

        for (Room room : booking.getRooms()) {
            if (!roomService.isRoomAvailable(room, booking.getDateFrom(), booking.getDateTo()))
                throw new NotAvailableRoomException(String.format("Room number %s is not available", room.getNumber()));
        }

        return bookingRepository.save(booking);
    }
}
