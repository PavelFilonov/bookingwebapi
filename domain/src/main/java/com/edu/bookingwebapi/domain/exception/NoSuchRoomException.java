package com.edu.bookingwebapi.domain.exception;

import java.util.NoSuchElementException;

public class NoSuchRoomException extends NoSuchElementException {
    public NoSuchRoomException(String message) {
        super(message);
    }
}
