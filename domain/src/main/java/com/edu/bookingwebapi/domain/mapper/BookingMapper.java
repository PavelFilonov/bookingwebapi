package com.edu.bookingwebapi.domain.mapper;

import com.edu.bookingwebapi.domain.entity.*;

public interface BookingMapper <T> {
    T map(Booking booking);
    Booking map(T obj);
}
