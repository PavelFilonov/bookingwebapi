package com.edu.bookingwebapi.domain.exception;

import java.util.NoSuchElementException;

public class NoSuchFeedbackException extends NoSuchElementException {
    public NoSuchFeedbackException(String s) {
        super(s);
    }
}
