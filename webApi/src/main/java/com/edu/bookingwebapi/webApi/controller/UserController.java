package com.edu.bookingwebapi.webApi.controller;

import br.com.fluentvalidator.context.ValidationResult;
import com.edu.bookingwebapi.domain.entity.Role;
import com.edu.bookingwebapi.domain.entity.User;
import com.edu.bookingwebapi.domain.exception.NoSuchUserException;
import com.edu.bookingwebapi.domain.service.UserService;
import com.edu.bookingwebapi.webApi.dto.UserDTO;
import com.edu.bookingwebapi.webApi.mapper.UserDTOMapper;
import com.edu.bookingwebapi.webApi.validator.UserValidator;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/users")
@Slf4j
@Api(description = "Маршруты для пользователей",
        tags = {"User"})
public class UserController {

    private final UserService userService;
    private final UserValidator userValidator;
    private final UserDTOMapper userDTOMapper;
    private final AuthenticationManager authenticationManager;
    private final PasswordEncoder passwordEncoder;

    @Value("${user.name.size.message}")
    private String isIncorrectSizeUsernameMessage;
    @Value("${user.name.size.min.value}")
    private Integer minSizeUsername;
    @Value("${user.name.size.max.value}")
    private Integer maxSizeUsername;

    @Value("${user.password.size.message}")
    private String isIncorrectSizePasswordMessage;
    @Value("${user.password.size.min.value}")
    private Integer minSizePassword;
    @Value("${user.password.size.max.value}")
    private Integer maxSizePassword;

    @Autowired
    public UserController(UserService userService, UserValidator userValidator, UserDTOMapper userDTOMapper,
                          AuthenticationManager authenticationManager, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.userValidator = userValidator;
        this.userDTOMapper = userDTOMapper;
        this.authenticationManager = authenticationManager;
        this.passwordEncoder = passwordEncoder;
    }

    @ApiOperation(value = "Получение авторизованного пользователя", tags = {"User"})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Пользователь получен")})
    @GetMapping("/profile")
    public ResponseEntity<?> get(Principal principal) {
        log.debug("Successful get request for /users/profile");

        UserDTO user = userDTOMapper.map(userService.findByUsername(principal.getName()));
        user.setPassword(null);

        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @ApiOperation(value = "Получение всех пользователей администратором", tags = {"User"})
    @ApiResponses(value = {
            @ApiResponse(code = 403, message = "Ошибка доступа к получению всех пользователей"),
            @ApiResponse(code = 200, message = "Пользователи получены"),
            @ApiResponse(code = 404, message = "Пользователи не найдены")})
    @GetMapping("/all")
    public ResponseEntity<?> getAll(Principal principal) {
        if (!userService.isAdmin(principal.getName())) {
            log.info("Forbidden get request for /users");
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        log.debug("Successful get request for /users");

        List<UserDTO> users = userDTOMapper.mapListToDTO(userService.findAll());
        return users != null && !users.isEmpty()
                ? new ResponseEntity<>(users, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @ApiOperation(value = "Получение пользователя по имени", tags = {"User"})
    @ApiResponses(value = {
            @ApiResponse(code = 403, message = "Ошибка доступа к получению пользователя"),
            @ApiResponse(code = 200, message = "Пользователь получен"),
            @ApiResponse(code = 404, message = "Пользователь не найден")})
    @GetMapping
    public ResponseEntity<?> getByUsername(Principal principal, @ApiParam("Имя пользователя") @RequestParam String username) {
        if (!userService.isAdmin(principal.getName())) {
            log.info("Forbidden get request for /users");
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        UserDTO user = userDTOMapper.map(userService.findByUsername(username));
        return user != null
                ? new ResponseEntity<>(user, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @ApiOperation(value = "Авторизация пользователя", tags = {"User"})
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Пользователь не найден"),
            @ApiResponse(code = 409, message = "Неверный пароль"),
            @ApiResponse(code = 200, message = "Авторизация прошла успешно")})
    @PostMapping("/login")
    public ResponseEntity<?> login(@ApiParam("Авторизируемый пользователь") @RequestBody UserDTO user) {
        log.debug("Successful post request for /users/login");

        User userFromDb = userService.findByUsername(user.getUsername());

        if (userFromDb == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        if (!passwordEncoder.matches(user.getPassword(), userFromDb.getPassword()))
            return new ResponseEntity<>(HttpStatus.CONFLICT);

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(userFromDb.getUsername(), user.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);

        log.debug("Authorization was successful");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ApiOperation(value = "Регистрация пользователя", tags = {"User"})
    @ApiResponses(value = {
            @ApiResponse(code = 422, message = "Невалидный пользователь. Возращается список ошибок валидации."),
            @ApiResponse(code = 409, message = "Имя уже используются"),
            @ApiResponse(code = 201, message = "Регистрация прошла успешно")})
    @PostMapping("/register")
    public ResponseEntity<?> register(@ApiParam("Новый пользователь") @RequestBody UserDTO user) {
        log.debug("Successful post request for /users");

        ValidationResult validationResult = userValidator.validate(user);
        if (!validationResult.isValid()) {
            log.debug("Invalid user: " + validationResult.getErrors());
            return new ResponseEntity<>(validationResult.getErrors(), HttpStatus.UNPROCESSABLE_ENTITY);
        }

        if (userService.existsByUsername(user.getUsername())) {
            log.debug("Username is already in use");
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRoles(Collections.singletonList(Role.USER));
        userService.save(userDTOMapper.map(user));
        log.debug("Registration was successful: " + user);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @ApiOperation(value = "Обновление имени пользователя", tags = {"User"})
    @ApiResponses(value = {
            @ApiResponse(code = 422, message = "Невалидное имя. Возращается информация об ошибке."),
            @ApiResponse(code = 409, message = "Логин уже используется"),
            @ApiResponse(code = 200, message = "Имя изменёно")})
    @PutMapping("/profile/username")
    public ResponseEntity<?> updateUsername(Principal principal,
                                            @ApiParam("Новое имя пользователя") @RequestParam String username) {
        log.debug("Successful put request for /users/profile/username");

        if (minSizeUsername > username.length() || username.length() > maxSizeUsername)
            return new ResponseEntity<>(isIncorrectSizeUsernameMessage, HttpStatus.UNPROCESSABLE_ENTITY);

        if (userService.existsByUsername(username))
            return new ResponseEntity<>(HttpStatus.CONFLICT);

        UserDTO userFromDb = userDTOMapper.map(userService.findByUsername(principal.getName()));
        userService.updateUsername(username, userFromDb.getId());
        log.debug("Username changed");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ApiOperation(value = "Обновление пароля пользователя", tags = {"User"})
    @ApiResponses(value = {
            @ApiResponse(code = 422, message = "Невалидный пароль. Возращается информация об ошибке."),
            @ApiResponse(code = 200, message = "Пароль изменён")})
    @PutMapping("/profile/password")
    public ResponseEntity<?> updatePassword(Principal principal,
                                            @ApiParam("Новый пароль") @RequestParam String password) {
        log.debug("Successful put request for /users/profile/password");

        if (minSizePassword > password.length() || password.length() > maxSizePassword)
            return new ResponseEntity<>(isIncorrectSizePasswordMessage, HttpStatus.UNPROCESSABLE_ENTITY);

        UserDTO userFromDb = userDTOMapper.map(userService.findByUsername(principal.getName()));
        userService.updatePassword(passwordEncoder.encode(password), userFromDb.getId());
        log.debug("Password changed");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ApiOperation(value = "Удаление пользователя по id", tags = {"User"})
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Пользователь не найден"),
            @ApiResponse(code = 403, message = "Ошибка доступа к удалению пользователя"),
            @ApiResponse(code = 200, message = "Пользователь удалён")})
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteById(Principal principal,
                                        @ApiParam("Id удаляемого пользователя") @PathVariable Long id) {
        User user;
        try {
            user = userService.findById(id);
        } catch (NoSuchUserException e) {
            log.debug(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }

        if (!principal.getName().equals(user.getUsername()) || !userService.isAdmin(principal.getName())) {
            log.info(String.format("Forbidden delete request for /users/%s", id));
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        log.debug(String.format("Successful delete request for /users/%s", id));

        userService.deleteById(id);
        log.debug(String.format("User with id %s was deleted", id));
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ApiOperation(value = "Удаление пользователя по имени", tags = {"User"})
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Пользователь не найден"),
            @ApiResponse(code = 403, message = "Ошибка доступа к удалению пользователя"),
            @ApiResponse(code = 200, message = "Пользователь удалён")})
    @DeleteMapping
    public ResponseEntity<?> deleteByUsername(Principal principal,
                                              @ApiParam("Логин удаляемого пользователя") @RequestParam String username) {
        if (!userService.existsByUsername(username))
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        if (!principal.getName().equals(username) || !userService.isAdmin(principal.getName())) {
            log.info("Forbidden delete request for /users");
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        log.debug("Successful delete request for /users");

        userService.deleteByUsername(username);
        log.debug(String.format("User with username %s was deleted", username));
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
