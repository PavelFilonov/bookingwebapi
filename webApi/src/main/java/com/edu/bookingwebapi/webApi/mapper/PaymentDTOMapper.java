package com.edu.bookingwebapi.webApi.mapper;

import com.edu.bookingwebapi.domain.entity.Payment;
import com.edu.bookingwebapi.domain.mapper.PaymentMapper;
import com.edu.bookingwebapi.webApi.dto.PaymentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Component
public class PaymentDTOMapper implements PaymentMapper<PaymentDTO> {

    private final UserDTOMapper userDTOMapper;
    private final BookingDTOMapper bookingDTOMapper;

    @Autowired
    public PaymentDTOMapper(UserDTOMapper userDTOMapper, BookingDTOMapper bookingDTOMapper) {
        this.userDTOMapper = userDTOMapper;
        this.bookingDTOMapper = bookingDTOMapper;
    }

    @Override
    public PaymentDTO map(Payment payment) {
        if (payment == null)
            return null;

        return PaymentDTO.builder()
                .id(payment.getId())
                .userDTO(userDTOMapper.map(payment.getUser()))
                .bookingDTO(bookingDTOMapper.map(payment.getBooking()))
                .totalCost(payment.getTotalCost())
                .createdAt(payment.getCreatedAt().toString())
                .build();
    }

    @Override
    public Payment map(PaymentDTO paymentDTO) {
        if (paymentDTO == null)
            return null;

        return Payment.builder()
                .id(paymentDTO.getId())
                .user(userDTOMapper.map(paymentDTO.getUserDTO()))
                .booking(bookingDTOMapper.map(paymentDTO.getBookingDTO()))
                .totalCost(paymentDTO.getTotalCost())
                .createdAt(LocalDateTime.parse(paymentDTO.getCreatedAt()))
                .build();
    }

    public List<PaymentDTO> mapListToDTO(List<Payment> payments) {
        List<PaymentDTO> paymentsDTO = new ArrayList<>();
        payments.forEach(payment -> paymentsDTO.add(map(payment)));
        return paymentsDTO;
    }
}
