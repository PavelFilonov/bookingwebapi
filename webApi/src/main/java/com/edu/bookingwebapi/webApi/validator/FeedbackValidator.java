package com.edu.bookingwebapi.webApi.validator;

import br.com.fluentvalidator.AbstractValidator;
import br.com.fluentvalidator.context.Error;
import br.com.fluentvalidator.context.ValidationResult;
import com.edu.bookingwebapi.webApi.dto.FeedbackDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Collection;

import static br.com.fluentvalidator.predicate.LogicalPredicate.not;
import static br.com.fluentvalidator.predicate.ObjectPredicate.nullValue;
import static br.com.fluentvalidator.predicate.StringPredicate.stringEmptyOrNull;

@Component
public class FeedbackValidator extends AbstractValidator<FeedbackDTO> {

    @Value("field.empty")
    private String isEmptyOrNullFieldMessage;

    @Override
    public ValidationResult validate(FeedbackDTO instance) {
        ValidationResult result = super.validate(instance);
        if (!result.isValid())
            return result;

        Collection<Error> errors = new ArrayList<>();
        try {
            LocalDateTime.parse(instance.getCreatedAt());
        } catch (DateTimeParseException e) {
            errors.add(Error.create("createdAt", e.getMessage(), null, null));
            return ValidationResult.fail(errors);
        }

        return ValidationResult.ok();
    }

    @Override
    public void rules() {
        ruleFor(FeedbackDTO::getUserDTO)
                .must(not(nullValue()))
                .withMessage(isEmptyOrNullFieldMessage)
                .withFieldName("user")
                .critical();

        ruleFor(FeedbackDTO::getRoomDTO)
                .must(not(nullValue()))
                .withMessage(isEmptyOrNullFieldMessage)
                .withFieldName("room")
                .critical();

        ruleFor(FeedbackDTO::getCreatedAt)
                .must(not(stringEmptyOrNull()))
                .withMessage(isEmptyOrNullFieldMessage)
                .withFieldName("createdAt")
                .critical();
    }
}
