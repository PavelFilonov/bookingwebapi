package com.edu.bookingwebapi.webApi.validator;

import br.com.fluentvalidator.AbstractValidator;
import br.com.fluentvalidator.context.Error;
import br.com.fluentvalidator.context.ValidationResult;
import com.edu.bookingwebapi.webApi.dto.BookingDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Collection;

import static br.com.fluentvalidator.predicate.LogicalPredicate.not;
import static br.com.fluentvalidator.predicate.StringPredicate.stringEmptyOrNull;

@Component
public class BookingValidator extends AbstractValidator<BookingDTO> {

    @Value("${date-from.invalid}")
    private String invalidDateFromMessage;

    @Value("${date-to.invalid}")
    private String invalidDateToMessage;

    @Value("${date.from-is-before-now}")
    private String isFromBeforeNowMessage;

    @Value("${date.from-is-after-to}")
    private String isFromAfterToMessage;

    @Value("${booking.interval.value}")
    private Integer minBookingIntervalValue;

    @Value("${booking.interval.message}")
    private String minBookingIntervalMessage;

    @Value("rooms.empty")
    private String emptyRoomsMessage;

    @Override
    public ValidationResult validate(BookingDTO instance) {
        ValidationResult result = super.validate(instance);
        if (!result.isValid())
            return result;

        Collection<Error> errors = new ArrayList<>();

        LocalDateTime dateFrom;
        LocalDateTime dateTo;

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        try {
            dateFrom = LocalDateTime.parse(instance.getDateFrom(), formatter);
            dateTo = LocalDateTime.parse(instance.getDateTo(), formatter);
        } catch (DateTimeParseException e) {
            errors.add(Error.create(null, e.getMessage(), null, null));
            return ValidationResult.fail(errors);
        }

        if (dateFrom.isBefore(LocalDateTime.now()))
            errors.add(Error.create(null, isFromBeforeNowMessage, null, null));

        if (dateFrom.isAfter(dateTo))
            errors.add(Error.create(null, isFromAfterToMessage, null, null));

        if (Duration.between(dateFrom, dateTo).toMinutes() < minBookingIntervalValue)
            errors.add(Error.create(null, minBookingIntervalMessage, null, null));

        if (instance.getRoomsDTO() == null || instance.getRoomsDTO().isEmpty())
            errors.add(Error.create("rooms", emptyRoomsMessage, null, null));

        if (errors.isEmpty())
            return ValidationResult.ok();

        return ValidationResult.fail(errors);
    }

    @Override
    public void rules() {
        ruleFor(BookingDTO::getDateFrom)
                .must(not(stringEmptyOrNull()))
                .withMessage(invalidDateFromMessage)
                .withFieldName("dateFrom")
                .critical();

        ruleFor(BookingDTO::getDateTo)
                .must(not(stringEmptyOrNull()))
                .withMessage(invalidDateToMessage)
                .withFieldName("dateTo")
                .critical();
    }
}
