package com.edu.bookingwebapi.webApi.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BookingDTO {
    private Long id;
    private String comment;
    private String dateFrom;
    private String dateTo;
    private UserDTO userDTO;
    private Collection<RoomDTO> roomsDTO;
}
