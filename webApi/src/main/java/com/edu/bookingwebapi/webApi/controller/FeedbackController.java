package com.edu.bookingwebapi.webApi.controller;

import br.com.fluentvalidator.context.ValidationResult;
import com.edu.bookingwebapi.domain.entity.Feedback;
import com.edu.bookingwebapi.domain.exception.NoSuchFeedbackException;
import com.edu.bookingwebapi.domain.service.FeedbackService;
import com.edu.bookingwebapi.domain.service.UserService;
import com.edu.bookingwebapi.webApi.dto.FeedbackDTO;
import com.edu.bookingwebapi.webApi.dto.RoomDTO;
import com.edu.bookingwebapi.webApi.mapper.FeedbackDTOMapper;
import com.edu.bookingwebapi.webApi.mapper.RoomDTOMapper;
import com.edu.bookingwebapi.webApi.validator.FeedbackValidator;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(value = "/feedbacks")
@Slf4j
@Api(description = "Маршруты для отзывов",
        tags = {"Feedback"})
public class FeedbackController {

    private final FeedbackService feedbackService;
    private final FeedbackDTOMapper feedbackDTOMapper;
    private final FeedbackValidator feedbackValidator;
    private final RoomDTOMapper roomDTOMapper;
    private final UserService userService;

    @Autowired
    public FeedbackController(FeedbackService feedbackService, FeedbackDTOMapper feedbackDTOMapper,
                              FeedbackValidator feedbackValidator, RoomDTOMapper roomDTOMapper, UserService userService) {
        this.feedbackService = feedbackService;
        this.feedbackDTOMapper = feedbackDTOMapper;
        this.feedbackValidator = feedbackValidator;
        this.roomDTOMapper = roomDTOMapper;
        this.userService = userService;
    }

    @ApiOperation(value = "Добавление отзыва", tags = {"Feedback"})
    @ApiResponses(value = {
            @ApiResponse(code = 422, message = "Невалидный отзыв. Возвращается список ошибок валидации"),
            @ApiResponse(code = 403, message = "Ошибка доступа. Пользователь в отзыве не соответствует текущему."),
            @ApiResponse(code = 201, message = "Отзыв создан")})
    @PostMapping
    public ResponseEntity<?> add(Principal principal, @ApiParam("Добавляемый отзыв") @RequestBody FeedbackDTO feedbackDTO) {
        log.debug("Successful post request for /feedbacks");

        ValidationResult validationResult = feedbackValidator.validate(feedbackDTO);
        if (!validationResult.isValid()) {
            log.warn("Invalid feedback: " + validationResult);
            return new ResponseEntity<>(validationResult.getErrors(), HttpStatus.UNPROCESSABLE_ENTITY);
        }

        if (!principal.getName().equals(feedbackDTO.getUserDTO().getUsername())) {
            log.info("Forbidden get request for /feedback");
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        feedbackService.save(feedbackDTOMapper.map(feedbackDTO));
        log.info("A new feedback has been created");
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @ApiOperation(value = "Получение отзывов по комнате", tags = {"Feedback"})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Отзывы получены"),
            @ApiResponse(code = 404, message = "Отзывы не найдены")})
    @GetMapping
    public ResponseEntity<?> getByRoom(@ApiParam("Комната") @RequestBody RoomDTO room) {
        log.debug("Successful get request for /feedbacks");

        List<FeedbackDTO> feedbacks = feedbackDTOMapper.mapListToDTO(feedbackService.findByRoom(roomDTOMapper.map(room)));
        return feedbacks != null && !feedbacks.isEmpty()
                ? new ResponseEntity<>(feedbacks, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @ApiOperation(value = "Удаление отзыва по id", tags = {"Feedback"})
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Отзыв не найден"),
            @ApiResponse(code = 403, message = "Ошибка доступа к удалению отзыва"),
            @ApiResponse(code = 200, message = "Отзыв удалён")})
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(Principal principal, @ApiParam("Id отзыва") @PathVariable Long id) {
        Feedback feedback;
        try {
            feedback = feedbackService.findById(id);
        } catch (NoSuchFeedbackException e) {
            log.warn(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }

        if (!principal.getName().equals(feedback.getUser().getUsername()) || !userService.isAdmin(principal.getName())) {
            log.info(String.format("Forbidden delete request for /feedbacks/%s", id));
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        log.debug(String.format("Successful delete request for /feedbacks/%s", id));

        feedbackService.delete(id);
        log.info(String.format("A feedback with id %s has been deleted", id));
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
