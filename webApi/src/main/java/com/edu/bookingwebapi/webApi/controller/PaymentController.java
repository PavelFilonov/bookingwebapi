package com.edu.bookingwebapi.webApi.controller;

import br.com.fluentvalidator.context.ValidationResult;
import com.edu.bookingwebapi.domain.service.PaymentService;
import com.edu.bookingwebapi.domain.service.UserService;
import com.edu.bookingwebapi.webApi.dto.PaymentDTO;
import com.edu.bookingwebapi.webApi.dto.UserDTO;
import com.edu.bookingwebapi.webApi.mapper.PaymentDTOMapper;
import com.edu.bookingwebapi.webApi.mapper.UserDTOMapper;
import com.edu.bookingwebapi.webApi.validator.PaymentValidator;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(value = "/payments")
@Slf4j
@Api(description = "Маршруты для платежей",
        tags = {"Payment"})
public class PaymentController {

    private final PaymentService paymentService;
    private final PaymentDTOMapper paymentDTOMapper;
    private final PaymentValidator paymentValidator;
    private final UserDTOMapper userDTOMapper;
    private final UserService userService;

    @Autowired
    public PaymentController(PaymentService paymentService, PaymentDTOMapper paymentDTOMapper,
                             PaymentValidator paymentValidator, UserDTOMapper userDTOMapper, UserService userService) {
        this.paymentService = paymentService;
        this.paymentDTOMapper = paymentDTOMapper;
        this.paymentValidator = paymentValidator;
        this.userDTOMapper = userDTOMapper;
        this.userService = userService;
    }

    @ApiOperation(value = "Получение платежей по пользователю", tags = {"Payment"})
    @ApiResponses(value = {
            @ApiResponse(code = 403, message = "Ошибка доступа к получению платежей"),
            @ApiResponse(code = 200, message = "Платежи получены"),
            @ApiResponse(code = 404, message = "Платежи не найдены")})
    @GetMapping
    public ResponseEntity<?> getByUser(Principal principal, @ApiParam("Пользователь") @RequestBody UserDTO user) {
        if (!principal.getName().equals(user.getUsername()) || !userService.isAdmin(principal.getName())) {
            log.info("Forbidden get request for /payments");
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        log.debug("Successful get request for /payments");

        List<PaymentDTO> payments = paymentDTOMapper.mapListToDTO(paymentService.findByUser(userDTOMapper.map(user)));
        return payments != null && !payments.isEmpty()
                ? new ResponseEntity<>(payments, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @ApiOperation(value = "Добавление платежа", tags = {"Payment"})
    @ApiResponses(value = {
            @ApiResponse(code = 422, message = "Невалидный платёж. Возвращается список ошибок валидации."),
            @ApiResponse(code = 201, message = "Платёж добавлен")})
    @PostMapping
    public ResponseEntity<?> add(@ApiParam("Добавляемый платёж") @RequestBody PaymentDTO payment) {
        log.debug("Successful post request for /payments");

        ValidationResult validationResult = paymentValidator.validate(payment);
        if (!validationResult.isValid()) {
            log.warn("Invalid payment: " + validationResult);
            return new ResponseEntity<>(validationResult.getErrors(), HttpStatus.UNPROCESSABLE_ENTITY);
        }

        paymentService.save(paymentDTOMapper.map(payment));
        log.info("A new payment has been created");
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
