package com.edu.bookingwebapi.webApi.controller;

import br.com.fluentvalidator.context.ValidationResult;
import com.edu.bookingwebapi.domain.exception.NoSuchRoomException;
import com.edu.bookingwebapi.domain.exception.NotAvailableRoomException;
import com.edu.bookingwebapi.domain.service.BookingService;
import com.edu.bookingwebapi.domain.service.UserService;
import com.edu.bookingwebapi.webApi.dto.BookingDTO;
import com.edu.bookingwebapi.webApi.mapper.BookingDTOMapper;
import com.edu.bookingwebapi.webApi.mapper.UserDTOMapper;
import com.edu.bookingwebapi.webApi.validator.BookingValidator;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/bookings")
@Slf4j
@Api(description = "Маршруты для бронирований",
        tags = {"Booking"})
public class BookingController {

    private final BookingService bookingService;
    private final BookingDTOMapper bookingDTOMapper;
    private final BookingValidator bookingValidator;
    private final UserService userService;
    private final UserDTOMapper userDTOMapper;

    @Autowired
    public BookingController(BookingService bookingService, BookingDTOMapper bookingDTOMapper,
                             BookingValidator bookingValidator, UserService userService, UserDTOMapper userDTOMapper) {
        this.bookingService = bookingService;
        this.bookingDTOMapper = bookingDTOMapper;
        this.bookingValidator = bookingValidator;
        this.userService = userService;
        this.userDTOMapper = userDTOMapper;
    }

    @ApiOperation(value = "Получение бронирований пользователя", tags = {"Booking"})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Бронирования получены"),
            @ApiResponse(code = 404, message = "Бронирования не найдены")})
    @GetMapping("/my")
    public ResponseEntity<?> get(Principal principal) {
        log.debug("Successful get request for /bookings");

        List<BookingDTO> bookings = bookingDTOMapper.mapListToDTO(bookingService.findBookingsByUser(userService.findByUsername(principal.getName())));
        return bookings != null && !bookings.isEmpty()
                ? new ResponseEntity<>(bookings, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @ApiOperation(value = "Получение бронирований по имени пользователя", tags = {"Booking"})
    @ApiResponses(value = {
            @ApiResponse(code = 403, message = "Ошибка доступа получения бронирований"),
            @ApiResponse(code = 200, message = "Бронирования получены"),
            @ApiResponse(code = 404, message = "Бронирования не найдены")})
    @GetMapping
    public ResponseEntity<?> getByUsername(Principal principal, @ApiParam("Имя пользователя") @RequestParam String username) {
        if (!principal.getName().equals(username) || !userService.isAdmin(principal.getName())) {
            log.info(String.format("Forbidden get request for /bookings?username=%s", username));
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        log.debug(String.format("Successful get request for /bookings?username=%s", username));

        List<BookingDTO> bookings = bookingDTOMapper.mapListToDTO(bookingService.findBookingsByUser(userService.findByUsername(username)));
        return bookings != null && !bookings.isEmpty()
                ? new ResponseEntity<>(bookings, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @ApiOperation(value = "Добавление бронирования", tags = {"Booking"})
    @ApiResponses(value = {
            @ApiResponse(code = 422, message = "Невалидное бронирование. Возвращается список ошибок валидации"),
            @ApiResponse(code = 409, message = "Ошибка доступности комнаты. Возвращается сообщение об ошибке."),
            @ApiResponse(code = 201, message = "Бронирование создано")})
    @PostMapping
    public ResponseEntity<?> add(Principal principal, @ApiParam("Добавляемое бронирование") @RequestBody BookingDTO booking) {
        log.debug("Successful post request for /bookings");

        booking.setUserDTO(userDTOMapper.map(userService.findByUsername(principal.getName())));

        ValidationResult validationResult = bookingValidator.validate(booking);
        if (!validationResult.isValid()) {
            log.warn("Invalid booking: " + validationResult);
            return new ResponseEntity<>(validationResult.getErrors(), HttpStatus.UNPROCESSABLE_ENTITY);
        }

        try {
            bookingService.save(bookingDTOMapper.map(booking));
        } catch (NoSuchRoomException | NotAvailableRoomException e) {
            log.warn(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }

        log.info("A new booking has been created");
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
