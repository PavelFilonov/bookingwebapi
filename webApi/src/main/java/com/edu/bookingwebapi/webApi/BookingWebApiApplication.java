package com.edu.bookingwebapi.webApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@EnableJpaRepositories("com.edu.bookingwebapi")
@EntityScan("com.edu.bookingwebapi")
@ComponentScan("com.edu.bookingwebapi")
@SpringBootApplication
public class BookingWebApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(BookingWebApiApplication.class, args);
    }
}
