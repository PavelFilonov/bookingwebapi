package com.edu.bookingwebapi.webApi.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FeedbackDTO {
    private Long id;
    private UserDTO userDTO;
    private RoomDTO roomDTO;
    private String description;
    private String createdAt;
}
