package com.edu.bookingwebapi.webApi.mapper;

import com.edu.bookingwebapi.domain.entity.Booking;
import com.edu.bookingwebapi.domain.mapper.BookingMapper;
import com.edu.bookingwebapi.webApi.dto.BookingDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class BookingDTOMapper implements BookingMapper<BookingDTO> {

    private final UserDTOMapper userDTOMapper;
    private final RoomDTOMapper roomDTOMapper;

    @Autowired
    public BookingDTOMapper(UserDTOMapper userDTOMapper, RoomDTOMapper roomDTOMapper) {
        this.userDTOMapper = userDTOMapper;
        this.roomDTOMapper = roomDTOMapper;
    }

    @Override
    public BookingDTO map(Booking booking) {
        if (booking == null)
            return null;

        return BookingDTO.builder()
                .id(booking.getId())
                .comment(booking.getComment())
                .dateFrom(booking.getDateFrom().toString())
                .dateTo(booking.getDateTo().toString())
                .userDTO(userDTOMapper.map(booking.getUser()))
                .roomsDTO(roomDTOMapper.mapListToDTO(booking.getRooms().stream().collect(Collectors.toList())))
                .build();
    }

    @Override
    public Booking map(BookingDTO bookingDTO) {
        if (bookingDTO == null)
            return null;

        return Booking.builder()
                .id(bookingDTO.getId())
                .comment(bookingDTO.getComment())
                .dateFrom(LocalDateTime.parse(bookingDTO.getDateFrom()))
                .dateTo(LocalDateTime.parse(bookingDTO.getDateTo()))
                .user(userDTOMapper.map(bookingDTO.getUserDTO()))
                .rooms(roomDTOMapper.mapListToEntity(bookingDTO.getRoomsDTO().stream().collect(Collectors.toList())))
                .build();
    }

    public List<BookingDTO> mapListToDTO(List<Booking> bookings) {
        List<BookingDTO> bookingsDTO = new ArrayList<>();
        bookings.forEach(booking -> bookingsDTO.add(map(booking)));
        return bookingsDTO;
    }

    public List<Booking> mapListToEntity(List<BookingDTO> bookingsDTO) {
        List<Booking> bookings = new ArrayList<>();
        bookingsDTO.forEach(bookingDTO -> bookings.add(map(bookingDTO)));
        return bookings;
    }
}
