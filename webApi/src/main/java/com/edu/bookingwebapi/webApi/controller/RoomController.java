package com.edu.bookingwebapi.webApi.controller;

import br.com.fluentvalidator.context.ValidationResult;
import com.edu.bookingwebapi.domain.exception.NoSuchRoomException;
import com.edu.bookingwebapi.domain.service.RoomService;
import com.edu.bookingwebapi.domain.service.UserService;
import com.edu.bookingwebapi.webApi.dto.RoomDTO;
import com.edu.bookingwebapi.webApi.mapper.RoomDTOMapper;
import com.edu.bookingwebapi.webApi.validator.RoomValidator;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/rooms")
@Slf4j
@Api(description = "Маршруты для номеров",
        tags = {"Room"})
public class RoomController {

    private final RoomService roomService;
    private final RoomValidator roomValidator;
    private final RoomDTOMapper roomDTOMapper;
    private final UserService userService;

    @Autowired
    public RoomController(RoomService roomService, RoomValidator roomValidator,
                          RoomDTOMapper roomDTOMapper, UserService userService) {
        this.roomService = roomService;
        this.roomValidator = roomValidator;
        this.roomDTOMapper = roomDTOMapper;
        this.userService = userService;
    }

    @ApiOperation(value = "Получение всех номеров", tags = {"Room"})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Номера получены"),
            @ApiResponse(code = 404, message = "Номера не найдены")})
    @GetMapping("/all")
    public ResponseEntity<?> getAll() {
        log.debug("Successful get request for /rooms");

        List<RoomDTO> rooms = roomDTOMapper.mapListToDTO(roomService.findAll());
        return rooms != null && !rooms.isEmpty()
                ? new ResponseEntity<>(rooms, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @ApiOperation(value = "Получение номера по id", tags = {"Room"})
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Номер не найден. Возвращается сообщение об ошибке."),
            @ApiResponse(code = 200, message = "Номер получен")})
    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@ApiParam("Id номера") @PathVariable Long id) {
        log.debug(String.format("Successful get request for /rooms/%s", id));

        RoomDTO room;
        try {
            room = roomDTOMapper.map(roomService.findById(id));
        } catch (NoSuchRoomException e) {
            log.warn(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(room, HttpStatus.OK);
    }

    @ApiOperation(value = "Получение номера по порядковому номеру", tags = {"Room"})
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Номер не найден. Возвращается сообщение об ошибке."),
            @ApiResponse(code = 200, message = "Номер получен")})
    @GetMapping
    public ResponseEntity<?> getByNumber(@ApiParam("Порядковый номер номера") @RequestParam Integer number) {
        log.debug("Successful get request for /rooms");

        RoomDTO room;
        try {
            room = roomDTOMapper.map(roomService.findByNumber(number));
        } catch (NoSuchRoomException e) {
            log.warn(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(room, HttpStatus.OK);
    }

    @ApiOperation(value = "Добавление номера", tags = {"Room"})
    @ApiResponses(value = {
            @ApiResponse(code = 403, message = "Ошибка доступа добавления номера"),
            @ApiResponse(code = 422, message = "Ошибка валидации номера. Возвращается список ошибок валидации."),
            @ApiResponse(code = 400, message = "Ошибка добавления номера. Возвращается сообщение об ошибке."),
            @ApiResponse(code = 201, message = "Номер добавлен")})
    @PostMapping
    public ResponseEntity<?> add(Principal principal, @ApiParam("Добавляемый номер") @RequestBody RoomDTO room) {
        if (!userService.isAdmin(principal.getName())) {
            log.info("Forbidden post request for /rooms");
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        log.debug("Successful post request for /rooms");

        ValidationResult validationResult = roomValidator.validate(room);
        if (!validationResult.isValid()) {
            log.warn("Invalid room: " + validationResult);
            return new ResponseEntity<>(validationResult.getErrors(), HttpStatus.UNPROCESSABLE_ENTITY);
        }

        try {
            roomService.save(roomDTOMapper.map(room));
        } catch (Exception e) {
            log.warn(e.toString());
            return new ResponseEntity<>(e.toString(), HttpStatus.BAD_REQUEST);
        }

        log.info(String.format("A new room number %s has been created", room.getNumber()));
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @ApiOperation(value = "Удаление номера", tags = {"Room"})
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Номер не найден"),
            @ApiResponse(code = 403, message = "Ошибка доступа к удалению номера"),
            @ApiResponse(code = 200, message = "Номер удалён")})
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(Principal principal, @ApiParam("Id удаляемого номера") @PathVariable Long id) {
        try {
            roomService.findById(id);
        } catch (NoSuchRoomException e) {
            log.warn(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }

        if (!userService.isAdmin(principal.getName())) {
            log.info(String.format("Forbidden delete request for /rooms/%s", id));
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        log.debug(String.format("Successful delete request for /rooms/%s", id));

        roomService.delete(id);
        log.info(String.format("A room with id %s has been deleted", id));
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ApiOperation(value = "Поиск номеров по фильтру", tags = {"Room"})
    @ApiResponses(value = {
            @ApiResponse(code = 422, message = "Ошибка валидации дат"),
            @ApiResponse(code = 200, message = "Номера получены"),
            @ApiResponse(code = 404, message = "Номера не найдены")})
    @GetMapping("/filtering")
    public ResponseEntity<?> getRoomsByFilter(@ApiParam("Количество комнат номера") @RequestParam(required = false) Integer numberRooms,
                                              @ApiParam("Дата начала бронирования") @RequestParam(required = false) String dateFrom,
                                              @ApiParam("Дата окончания бронирования") @RequestParam(required = false) String dateTo) {
        log.debug("Successful get request for /rooms/filtering");

        Set<RoomDTO> rooms = new HashSet<>();

        if (numberRooms != null)
            rooms.addAll(roomDTOMapper.mapListToDTO(roomService.findByNumberRooms(numberRooms)));

        if (dateFrom != null && dateTo != null) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            LocalDateTime from;
            LocalDateTime to;
            try {
                from = LocalDateTime.parse(dateFrom, formatter);
                to = LocalDateTime.parse(dateTo, formatter);
            } catch (DateTimeParseException e) {
                log.warn("Invalid dates: " + e.getMessage());
                return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
            }

            rooms.addAll(roomDTOMapper.mapListToDTO(roomService.findAvailableRooms(from, to)));
        }

        return !rooms.isEmpty()
                ? new ResponseEntity<>(new ArrayList<>(rooms), HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
