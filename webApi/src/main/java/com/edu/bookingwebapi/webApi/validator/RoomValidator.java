package com.edu.bookingwebapi.webApi.validator;

import br.com.fluentvalidator.AbstractValidator;
import com.edu.bookingwebapi.webApi.dto.RoomDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import static br.com.fluentvalidator.predicate.ComparablePredicate.greaterThan;
import static br.com.fluentvalidator.predicate.ComparablePredicate.greaterThanOrEqual;
import static br.com.fluentvalidator.predicate.LogicalPredicate.not;
import static br.com.fluentvalidator.predicate.ObjectPredicate.nullValue;
import static br.com.fluentvalidator.predicate.StringPredicate.stringEmptyOrNull;

@Component
public class RoomValidator extends AbstractValidator<RoomDTO> {

    @Value("${number.positive}")
    private String isNotPositiveNumberMessage;

    @Value("${number.null}")
    private String isNullNumberMessage;

    @Value("field.empty")
    private String isEmptyOrNullFieldMessage;

    @Value("number.not-negative")
    private String isNegativeNumberMessage;

    @Override
    public void rules() {
        ruleFor(RoomDTO::getNumber)
                .must(not(nullValue()))
                    .withMessage(isNullNumberMessage)
                    .withFieldName("number")
                    .critical()
                .must(greaterThan(0))
                    .withMessage(isNotPositiveNumberMessage)
                    .withFieldName("number")
                    .critical();

        ruleFor(RoomDTO::getNumberRooms)
                .must(not(nullValue()))
                .withMessage(isNullNumberMessage)
                .withFieldName("numberRooms")
                .critical()
                .must(greaterThan(0))
                .withMessage(isNotPositiveNumberMessage)
                .withFieldName("numberRooms")
                .critical();

        ruleFor(RoomDTO::getCost)
                .must(not(nullValue()))
                .withMessage(isEmptyOrNullFieldMessage)
                .withFieldName("cost")
                .critical()
                .must(greaterThanOrEqual(0.))
                .withMessage(isNegativeNumberMessage)
                .withFieldName("numberRooms")
                .critical();
    }
}
