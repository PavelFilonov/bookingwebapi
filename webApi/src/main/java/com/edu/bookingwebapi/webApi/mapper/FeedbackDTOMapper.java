package com.edu.bookingwebapi.webApi.mapper;

import com.edu.bookingwebapi.domain.entity.Feedback;
import com.edu.bookingwebapi.domain.mapper.FeedbackMapper;
import com.edu.bookingwebapi.webApi.dto.FeedbackDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Component
public class FeedbackDTOMapper implements FeedbackMapper<FeedbackDTO> {

    private final UserDTOMapper userDTOMapper;
    private final RoomDTOMapper roomDTOMapper;

    @Autowired
    public FeedbackDTOMapper(UserDTOMapper userDTOMapper, RoomDTOMapper roomDTOMapper) {
        this.userDTOMapper = userDTOMapper;
        this.roomDTOMapper = roomDTOMapper;
    }

    @Override
    public FeedbackDTO map(Feedback feedback) {
        if (feedback == null)
            return null;

        return FeedbackDTO.builder()
                .id(feedback.getId())
                .userDTO(userDTOMapper.map(feedback.getUser()))
                .roomDTO(roomDTOMapper.map(feedback.getRoom()))
                .description(feedback.getDescription())
                .createdAt(feedback.getCreatedAt().toString())
                .build();
    }

    @Override
    public Feedback map(FeedbackDTO feedbackDTO) {
        if (feedbackDTO == null)
            return null;

        return Feedback.builder()
                .id(feedbackDTO.getId())
                .user(userDTOMapper.map(feedbackDTO.getUserDTO()))
                .room(roomDTOMapper.map(feedbackDTO.getRoomDTO()))
                .description(feedbackDTO.getDescription())
                .createdAt(LocalDateTime.parse(feedbackDTO.getCreatedAt()))
                .build();
    }

    public List<FeedbackDTO> mapListToDTO(List<Feedback> feedbacks) {
        List<FeedbackDTO> feedbacksDTO = new ArrayList<>();
        feedbacks.forEach(feedback -> feedbacksDTO.add(map(feedback)));
        return feedbacksDTO;
    }
}
