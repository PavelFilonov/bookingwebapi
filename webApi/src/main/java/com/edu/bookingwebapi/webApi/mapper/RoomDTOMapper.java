package com.edu.bookingwebapi.webApi.mapper;

import com.edu.bookingwebapi.domain.entity.Room;
import com.edu.bookingwebapi.domain.mapper.RoomMapper;
import com.edu.bookingwebapi.webApi.dto.RoomDTO;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class RoomDTOMapper implements RoomMapper<RoomDTO> {

    @Override
    public RoomDTO map(Room room) {
        if (room == null)
            return null;

        return RoomDTO.builder()
                .id(room.getId())
                .number(room.getNumber())
                .numberRooms(room.getNumberRooms())
                .description(room.getDescription())
                .cost(room.getCost())
                .build();
    }

    @Override
    public Room map(RoomDTO roomDTO) {
        if (roomDTO == null)
            return null;

        return Room.builder()
                .id(roomDTO.getId())
                .number(roomDTO.getNumber())
                .numberRooms(roomDTO.getNumberRooms())
                .description(roomDTO.getDescription())
                .cost(roomDTO.getCost())
                .build();
    }

    public List<RoomDTO> mapListToDTO(List<Room> rooms) {
        List<RoomDTO> roomsDTO = new ArrayList<>();
        rooms.forEach(room -> roomsDTO.add(map(room)));
        return roomsDTO;
    }

    public List<Room> mapListToEntity(List<RoomDTO> roomsDTO) {
        List<Room> rooms = new ArrayList<>();
        roomsDTO.forEach(roomDTO -> rooms.add(map(roomDTO)));
        return rooms;
    }
}
