package com.edu.bookingwebapi.webApi.validator;

import br.com.fluentvalidator.AbstractValidator;
import com.edu.bookingwebapi.webApi.dto.PaymentDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import static br.com.fluentvalidator.predicate.LogicalPredicate.not;
import static br.com.fluentvalidator.predicate.ObjectPredicate.nullValue;
import static br.com.fluentvalidator.predicate.StringPredicate.stringEmptyOrNull;

@Component
public class PaymentValidator extends AbstractValidator<PaymentDTO> {

    @Value("field.empty")
    private String isEmptyOrNullFieldMessage;

    @Override
    public void rules() {
        ruleFor(PaymentDTO::getUserDTO)
                .must(not(nullValue()))
                .withMessage(isEmptyOrNullFieldMessage)
                .withFieldName("user")
                .critical();

        ruleFor(PaymentDTO::getBookingDTO)
                .must(not(nullValue()))
                .withMessage(isEmptyOrNullFieldMessage)
                .withFieldName("booking")
                .critical();

        ruleFor(PaymentDTO::getTotalCost)
                .must(not(nullValue()))
                .withMessage(isEmptyOrNullFieldMessage)
                .withFieldName("totalCost")
                .critical();

        ruleFor(PaymentDTO::getCreatedAt)
                .must(not(stringEmptyOrNull()))
                .withMessage(isEmptyOrNullFieldMessage)
                .withFieldName("createdAt")
                .critical();
    }
}
