package com.edu.bookingwebapi.webApi.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PaymentDTO {
    private Long id;
    private UserDTO userDTO;
    private BookingDTO bookingDTO;
    private Double totalCost;
    private String createdAt;
}
