package com.edu.bookingwebapi.webApi.mapper;

import com.edu.bookingwebapi.domain.entity.User;
import com.edu.bookingwebapi.domain.mapper.UserMapper;
import com.edu.bookingwebapi.webApi.dto.UserDTO;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserDTOMapper implements UserMapper<UserDTO> {

    @Override
    public UserDTO map(User user) {
        if (user == null)
            return null;

        return UserDTO.builder()
                .id(user.getId())
                .username(user.getUsername())
                .password(user.getPassword())
                .roles(user.getRoles())
                .build();
    }

    @Override
    public User map(UserDTO userDTO) {
        if (userDTO == null)
            return null;

        return User.builder()
                .id(userDTO.getId())
                .username(userDTO.getUsername())
                .password(userDTO.getPassword())
                .roles(userDTO.getRoles())
                .build();
    }

    public List<UserDTO> mapListToDTO(List<User> users) {
        List<UserDTO> usersDTO = new ArrayList<>();
        users.forEach(user -> usersDTO.add(map(user)));
        return usersDTO;
    }
}
